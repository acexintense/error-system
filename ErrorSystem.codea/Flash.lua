Flash = class()

function Flash:init(message, status)
    -- you can accept and set parameters here
    self.width = WIDTH
    self.height = 50
    self.message = message
    self.status = status
    height = 0
    speed = 3
end

function Flash:draw()
    fill(50, 50, 50);
    if height <= self.height then
        height = height + speed
    end
    
    if self.status == 'Error' then
        fill(187, 33, 32, 255)
    elseif self.status == 'Warning' then
        fill(255, 189, 0, 255)
    elseif self.status == 'Success' then
        fill(135, 255, 0, 255)
    else
        self.status = 'Error'
    end
    
    noStroke()
    
    rect(0, HEIGHT - height, WIDTH, height)
    
    fill(30, 30, 30, 255)
    textAlign(LEFT)
    text(self.message, WIDTH / 2, HEIGHT - height + 25)
end
